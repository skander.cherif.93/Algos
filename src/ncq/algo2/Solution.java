package ncq.algo2;

public class Solution {
    public int[] solution(int[] a, int[] b) {
        final int[] step = new int[a.length < 2 ? 2 : a.length];
        step[0] = 1;
        step[1] = 2;
        for (int i = 2; i < a.length; i++) {
        	step[i] = (step[i - 1] + step[i - 2]) %((1 << 30));
        }
        for (int i = 0; i < a.length; i++) {
            a[i] = step[a[i] - 1] % ((1 << b[i]));
        }
        return a;
    }

}