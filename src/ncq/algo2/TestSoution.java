package ncq.algo2;

import java.util.Arrays;

public class TestSoution {

	public static void main(String[] args) {
		//Testing with values gived in algo1, just run as java application
		int[] A = new int[]{4, 4, 5, 5, 1};
		int[] B = new int[]{3, 2, 4, 3, 1};
		Solution sol=new Solution();
		int[] res=sol.solution(A, B);
		System.out.println("result = "+Arrays.toString(res));
	
	}
	}

