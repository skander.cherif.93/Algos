package ncq.algo1;

import java.util.Arrays;

public class TestSolution {

	public static void main(String[] args) {
		//Testing with values gived in algo1, just run as java application
		int N=5;
		int[] A = new int[]{3, 4, 4, 6, 1, 4, 4};
		System.out.println("result = "+Arrays.toString(Solution.solution(N, A)));
	
	}

}
