package ncq.algo1;

public class Solution {
	private int[] counters;
	private int biggestValInCounter = 0;
	private int currentScore = 0;

	public Solution(int length) {
		counters = new int[length];
	}

	public void MaxAllCounters() {
		currentScore = biggestValInCounter;
	}

	public void IncreaseCounter(int counterPosition) {
		counterPosition--;
		if (counters[counterPosition] < currentScore)
			counters[counterPosition] = currentScore + 1;
		else
			counters[counterPosition]++;
		if (counters[counterPosition] > biggestValInCounter)
			biggestValInCounter = counters[counterPosition];
	}

	public int[] ToArray() {
		for (int i = 0; i < counters.length; i++) {
			if (counters[i] < currentScore)
				counters[i] = currentScore;
		}
		return (int[]) counters.clone();
	}
	
	//solution
	public static int[] solution(int N, int[] A) {
		Solution counters = new Solution(N);
		for (int k = 0; k < A.length; k++) {
			if (A[k] <= N)
				counters.IncreaseCounter(A[k]);
			else
				counters.MaxAllCounters();
		}

		return counters.ToArray();
	}

}
